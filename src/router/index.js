import { createRouter, createWebHashHistory } from 'vue-router'
import UploadImage from '../views/UploadImage.vue'

const routes = [
    {path: '/', name:'Home', redirect: '/upload_image'},
    {path: '/upload_image', name:'Upload Image', component: UploadImage}
]

const router = createRouter({
    history: createWebHashHistory(process.env.BASE_URL),
    routes
})

export default router